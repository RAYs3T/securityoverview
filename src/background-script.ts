const testMessage: string = "Hello World!";
browser.browserAction.onClicked.addListener(
    () => console.log(testMessage),
);

browser.tabs.onCreated.addListener((tab) => {
    console.log("Tab URL: ", tab.url);
});

browser.webNavigation.onCommitted.addListener((queryEvent) => {
    console.log("Navigation: ", queryEvent.url);
});

function parseUrl(url: string) {
    let domain = null;
    let ssl = false;
    let ws = false;

    const a = document.createElement("a");
    a.href = url;
    if (a.protocol === "file:") {
        domain = "file://";
    } else if (a.protocol === "chrome:") {
        domain = "chrome://";
    } else {
        domain = a.hostname || "";
        switch (a.protocol) {
            case "https:":
                ssl = true;
                break;
            case "wss:":
                ssl = true;
            // fallthrough
            case "ws:":
                ws = true;
                break;
        }
    }
    return { domain, ssl, ws, origin: a.origin };
}
