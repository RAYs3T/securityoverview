enum TabState {
    TAB_BIRTH= 0,
    TAB_ALIVE= 1,
    TAB_DELETED = 2,

}

interface ITabInfo {
    tabId: number;
    url: string;
    state: TabState;
    mainDomain: string;
    mainOrigin: string;
    dataExists: boolean;
    committed: boolean;
    domains: [];
    spillCount: number;
    lastPattern: string;
    lastTooltip: string;
    accessDenied: boolean;
}
